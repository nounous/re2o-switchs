; {{ switch.model.reference }}A Configuration Editor; Created on release #{{ switch.model.firmware }}

hostname "{{ switch.short_name }}"
; Generated on {{ date_gen }} by re2o
;--- Snmp ---
{%- if switch.switchbay.name %}
snmp-server location "{{ switch.switchbay.name }}"
{%- endif %}
;A faire à la main
snmpv3 enable
snmpv3 restricted-access
snmpv3 user "re2o"
snmpv3 group ManagerPriv user "re2o" sec-model ver3
snmp-server community "public" Operator
;--- Heure/date
time timezone 60
time daylight-time-rule Western-Europe
{%- for ipv4 in settings.switchs_management_utils.ntp_servers.ipv4 %}
sntp server priority {{ loop.index }} {{ ipv4 }} 4
{%- endfor %}
{%- for ipv6 in settings.switchs_management_utils.ntp_servers.ipv6 %}
sntp server priority {{ loop.index + settings.switchs_management_utils.ntp_servers.ipv4|length }} {{ ipv6 }} 4
{%- endfor %}
timesync sntp
sntp unicast
;--- Misc ---
console inactivity-timer 30
;--- Logs ---
{%- for ipv4 in settings.switchs_management_utils.log_servers.ipv4 %}
logging {{ ipv4 }}
{%- endfor %}
{%- for ipv6 in settings.switchs_management_utils.log_servers.ipv6 %}
logging {{ ipv6 }}
{%- endfor %}
;--- IP du switch ---
no ip default-gateway
max-vlans 256
{%- for id, vlan in additionals.vlans.items() %}
vlan {{ id }}
   name "{{ vlan["name"]|capitalize }}"
   {%- if vlan["ports_tagged"] %}
   tagged {{ vlan["ports_tagged"]|join(',') }}
   {%- endif %}
   {%- if vlan["ports_untagged"] %}
   untagged {{ vlan["ports_untagged"]|join(',') }}
   {%- endif %} 
   {%- if id in additionals.igmp_vlans %}
   ip igmp
   {%- endif %}
   {%- if id in additionals.mld_vlans %}
   ipv6 mld version 1
   ipv6 mld enable
   {%- endif %}
   {%- if vlan.ipv4 %}
   {%- for ipv4, subnet in vlan.ipv4.items() %}
   ip address {{ ipv4 }}/{{ subnet.0.netmask_cidr }}
   {%- endfor %}
   {%- else %}
   no ip address
   {%- endif %} 
   {%- if vlan.ipv6 %}
   {%- for ipv6, subnet6 in vlan.ipv6.items() %}
   ipv6 address {{ ipv6 }}/{{ subnet6.netmask_cidr }}
   {%- endfor %}
   {%- if id in additionals.igmp_vlans %}
   no ip igmp querier
   {%- endif %} 
   {%- if id in additionals.mld_vlans %}
   no ipv6 mld querier
   {%- endif %} 
   {%- endif %} 
exit
{%- endfor %}
;--- Accès d'administration ---
no telnet-server
{%- if switch.web_management_enabled %}
{%- if switch.web_management_enabled != "ssl" %}
web-management plaintext
{%- endif %}
{%- if switch.web_management_enabled == "ssl" %}
web-management ssl
{%- endif %}
{%- else %}
no web-management
{%- endif %}
{%- if switch.rest_enabled %}
rest-interface
{%- endif %}
aaa authentication ssh login public-key none
aaa authentication ssh enable public-key none
ip ssh
ip ssh filetransfer
{%- if settings.switchs_management_utils.subnet %}
ip authorized-managers {{ settings.switchs_management_utils.subnet.0.network }} {{ settings.switchs_management_utils.subnet.0.netmask }} access manager
{%- endif %}
{%- if settings.switchs_management_utils.subnet6 %}
ipv6 authorized-managers {{ settings.switchs_management_utils.subnet6.network }} {{ settings.switchs_management_utils.subnet6.netmask }} access manager
{%- endif %}
{%- if additionals.loop_protected %}
;--- Protection contre les boucles ---
loop-protect disable-timer 30
loop-protect transmit-interval 3
loop-protect {{ additionals.loop_protected|join(',') }}
{%- endif %}
;--- Serveurs Radius 
radius-server dead-time 2
{%- for ipv4 in settings.switchs_management_utils.radius_servers.ipv4 %}
radius-server host {{ ipv4 }} key "{{ switch.get_radius_key_value }}"
radius-server host {{ ipv4 }} dyn-authorization
{%- endfor %}
radius-server dyn-autz-port 3799
;--- Filtrage mac ---
aaa port-access mac-based addr-format multi-colon
;--- Bricoles ---
no cdp run
{%- if additionals.dhcp_snooping_vlans %}
;--- DHCP Snooping ---
{%- for ipv4 in settings.switchs_management_utils.dhcp_servers.ipv4 %}
dhcp-snooping authorized-server {{ ipv4 }}
{%- endfor %}
dhcp-snooping vlan {{ additionals.dhcp_snooping_vlans|join(' ') }}
dhcp-snooping
{%- endif %}
{%- if additionals.arp_protect_vlans %}
;--- ARP Protect ---
arp-protect
arp-protect vlan {{ additionals.arp_protect_vlans|join(' ') }}
arp-protect validate src-mac dest-mac
{%- endif %}
{%- if additionals.dhcpv6_snooping_vlans %}
;--- DHCPv6 Snooping ---
dhcpv6-snooping vlan {{ additionals.dhcpv6_snooping_vlans|join(' ') }}
dhcpv6-snooping
{%- endif %}
{%- if additionals.ra_guarded %}
;--- RA guards ---
ipv6 ra-guard ports {{ additionals.ra_guarded|join(',')}}
{%- endif %}
;--- Config des prises ---
{%- for port in switch.ports %}
{%- if port.get_port_profil.radius_type == "802.1X" %}
aaa port-access authenticator {{ port.port }}
{%- if port.get_port_profil.mac_limit %}
aaa port-access authenticator {{ port.port }} client-limit {{ port.get_port_profil.mac_limit }}
{%- endif %}
aaa port-access authenticator {{ port.port }} logoff-period 3600
{%- endif %}
{%- if port.get_port_profil.radius_type == "MAC-radius" %}
aaa port-access mac-based {{ port.port }}
{%- if port.get_port_profil.mac_limit %}
aaa port-access mac-based {{ port.port }} addr-limit {{ port.get_port_profil.mac_limit }}
{%- endif %}
aaa port-access mac-based {{ port.port }} logoff-period 3600
aaa port-access mac-based {{ port.port }} unauth-vid 1
{%- endif %}
interface {{ port.port }}
   {%- if port.state %}
   enable
   {%- else %}
   disable
   {%- endif %}
   name "{{ port.pretty_name }}"
   {%- if port.get_port_profil.flow_control %}
   flow-control
   {%- endif %}
   {%- if not port.get_port_profil.dhcp_snooping %}
   dhcp-snooping trust
   {%- endif %}
   {%- if not port.get_port_profil.arp_protect %}
   arp-protect trust
   {%- endif %}
   {%- if not port.get_port_profil.dhcpv6_snooping %}
   dhcpv6-snooping trust
   {%- endif %}
   no lacp
exit
{%- endfor %}
;--- Configuration comptabilisation RADIUS ---
aaa accounting network start-stop radius
aaa accounting session-id unique
aaa accounting update periodic 240
;--- Filtre de protocole ---
filter multicast 01005e0000fb drop all
filter multicast 3333000000fb drop all
